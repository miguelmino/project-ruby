json.array!(@activos) do |activo|
  json.extract! activo, :id, :code, :name, :Description, :Department, :Manager, :invoice_number, :model, :date_admission, :state
  json.url activo_url(activo, format: :json)
end
