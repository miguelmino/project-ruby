class CreateActivos < ActiveRecord::Migration
  def change
    create_table :activos do |t|
      t.string :code
      t.string :name
      t.text :Description
      t.string :Department
      t.string :Manager
      t.integer :invoice_number
      t.string :model
      t.date :date_admission
      t.integer :state

      t.timestamps null: false
    end
  end
end
